/**********************************************************************
				
				 softening.c

	 This file contains functions/filters to soften images 
	 * average filter
	 * median filter 
	 * gaussian filter
	 
	 All functions takes an input image and return an output proccesed
	 image with same dimensions 

**********************************************************************/
/*Include Libraries*/
#include "Functions.h"

/* Applies an average filter of size*size dimensions to imagein, retunrs image out*/
int average(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int size){
    unsigned char *kernel;
    int j, i, ik, jk, pixel, sum, in, jn;
    
	/* Validate if size of kernel fits on image, and allocate memory for it. */
	if(size%2 == 0){
		printf("Invalid size of Kernel, Kernel must be odd.\n");
		return 0;
	} else if(size > rows || size > cols){
		printf("Invalid size of Kernel, Kernel cannot be greater than image.\n");
		return 0; 
	}
		
	/* Allocate memory for output image. */
	if(((*imageout) = (unsigned char *) malloc((rows)*(cols))) == NULL){
	  printf("Memory allocation failure.\n");
	  return(0);
	}
	
    	/*Writes new pixel values to output image*/
    	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			/*Verify if current pixel has space to apply kernel, if not, copy same pixel value*/
			if(i<(size/2) || i>=(rows-(size/2)) || j<(size/2) || j>=(cols-(size/2))){
 				(*imageout)[i*cols+j]=imagein[i*cols+j];
			} else {
				sum = 0;
				/*Get sum of pixels for average Kernel*/
				for (ik=(int)(-size/2);ik<=(int)(size/2);ik++){
					for (jk=(int)(-size/2);jk<=(int)(size/2);jk++){
						in=i+ik;
						jn=j+jk;
						pixel = imagein[in*cols+jn];
						sum= pixel+sum;		
					}
				}			
				(*imageout)[i*cols+j]=(unsigned char)(sum/(size*size));
			}
		}
	}
	return(1);
}

/* Applies an median filter of size*size dimensions to imagein, retunrs image out*/
int median(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int size){
    unsigned char *kernel;
    int i, j, k, l, n, ik, jk, pixel, in, jn, min;

	/* Validate if size of kernel fits on image, and allocate memory for it. */
	if(size%2 == 0){
		printf("Invalid size of Kernel, Kernel must be odd.\n");
		return 0;
	}else if(size < rows && size < cols){
		/* Allocate memory for output image and kernel. */
		if(((kernel) = (unsigned char *) malloc((size)*(size))) == NULL){
			printf("Memory allocation failure.\n");
			return(0);
		}
		if(((*imageout) = (unsigned char *) malloc((rows)*(cols))) == NULL){
		  	printf("Memory allocation failure.\n");
		  	return(0);
		}
	} else {
		printf("Invalid size of Kernel, Kernel cannot be greater than image.\n");
		return 0; 
	}
	
    	/*Writes new pixel values to output image*/
   	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			/*Verify if current pixel has space to apply kernel, if not, copy same pixel value*/
			if(i<(size/2) || i>=(rows-(size/2)) || j<(size/2) || j>=(cols-(size/2))){
 				(*imageout)[i*cols+j]=imagein[i*cols+j];
			} else {
				/*Fill median Kernel with pixels values*/
				for (ik=0;ik<size;ik++){
					for (jk=0;jk<size;jk++){
						in=i+ik-size/2;
						jn=j+jk-size/2;
						kernel[ik*size+jk]= imagein[in*cols+jn];		
					}
				}
				/*Order kernel values in asendent order*/
				for(n=0;n<(size*size);n++){
					min=kernel[n];
					for(l=n;l<(size*size);l++){
						if(kernel[l]<min){
							min=kernel[l];	
							kernel[l]=kernel[n];
							kernel[n]=min;
						}
					}
				}
				/*writes the new pixel value for output image*/		
				(*imageout)[i*cols+j]=kernel[(size*size)/2];
			}
		}
	}
	free(kernel);
    	return(1);
}

/* Applies a gaussian filter of size*size dimensions to imagein, retunrs image out*/
int gaussian(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int size, float stddev){
	double sumk, sum, pixel, *kernel;
	int i, j, k, l, n, ik, jk, in, jn;


	/* Validate if size of kernel fits on image, and allocate memory for it. */
	if(size%2 == 0){
		printf("Invalid size of Kernel, Kernel must be odd.\n");
		return 0;
	}else if(size < rows && size < cols){
		/* Allocate memory for output image and kernel. */
		if(((kernel) = (double *) malloc(sizeof(double)*(size)*(size))) == NULL){
			printf("Memory allocation failure.\n");
			return(0);
		}
		if(((*imageout) = (unsigned char *) malloc((rows)*(cols))) == NULL){
		  	printf("Memory allocation failure.\n");
		  	return(0);
		}
	} else {
		printf("Invalid size of Kernel, Kernel cannot be greater than image.\n");
		return 0; 
	}
	
	/*Generates gaussian Kernel*/
	sum = 0;
	for(ik=(int)(-size/2);ik<=(int)(size/2);ik++){
		for(jk=(int)(-size/2);jk<=(int)(size/2);jk++){
			in=ik+size/2;
			jn=jk+size/2;
			kernel[in*size+jn] = (exp(-1.*(ik*ik+jk*jk)/(2.*stddev*stddev)))/(2.*M_PI*stddev*stddev);
			sumk=sumk + kernel[in*size+jn];		
		}
	}

    	/*Writes new pixel values to output image*/
   	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			/*Verify if current pixel has space to apply kernel, if not, copy same pixel value*/
			if(i<(size/2) || i>=(rows-(size/2)) || j<(size/2) || j>=(cols-(size/2))){
 				(*imageout)[i*cols+j]=imagein[i*cols+j];
			} else {
				sum = 0;
				/*Get sum of pixels appling gaussian Kernel*/
				for (ik=0;ik<size;ik++){
					for (jk=0;jk<size;jk++){
						in=i+ik-size/2;
						jn=j+jk-size/2;
						pixel = imagein[in*cols+jn]*kernel[ik*size+jk];
						sum= pixel+sum;		
					}
				}
				/*writes the new pixel value for output image*/
				(*imageout)[i*cols+j]=(unsigned char)(sum/sumk);					
			}
		}
	}

	free(kernel);	
    	return(1);
}

/* End of File */
