/**********************************************************************
				
				 dif_functions.c

	 This file contains functions to compare two images
	 * absdiff - to calculate the absolute difference 
	 * erms - to calculate mean square error

	 All functions takes two images of same size as input and 
	 returns the value of the comparision.

**********************************************************************/
/*Include Libraries*/
#include "Functions.h"

/* Return an image that contains the absolut difference between two input images*/
int absdiff(unsigned char *imagein, int rows, int cols, unsigned char *imagein2, unsigned char **error){
    	int j, i, pixel;
    
	/* Allocate memory to store the difference image. */
	if(((*error) = (unsigned char *) malloc((rows)*(cols))) == NULL){
	  printf("Memory allocation failure.\n");
	  return(0);
	}
	
	/*Gets absoulte difference between images, write it on output image*/
	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			pixel = imagein[i*cols+j]-imagein2[i*cols+j];
			if(pixel<0){pixel=-1*pixel;}
			(*error)[i*cols+j]=pixel;
		}
	}
   
    return(1);
}

/* Calculates the mean square error of two given images*/
double erms(unsigned char *imagein, int rows, int cols, unsigned char *imagein2){
    	int j, i;
	double pixel,sum,error;
	
	/*Gets Erms*/
	sum = 0;
	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			pixel = (imagein[i*cols+j]-imagein2[i*cols+j]);
			pixel = pixel * pixel;
			sum = sum + pixel;
		}
	}
	error = (sqrt(sum)/(cols*rows));
   
    return(error);
}

/* End of File */
