/**********************************************************************
		Header that includes all the functions 
		structures to be used.
**********************************************************************/
/*Include Libraries*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define PIXEL_MAX_VAL 	255
#define PIXEL_MIN_VAL 	0
#define KERNEL90 	0
#define KERNEL45	1 

/* Read the contents of the input image file in PGM format*/
int read_pgm_image(char *infilename, unsigned char **image, int *rows, int *cols);

/* Write the output image file in PGM format */
int write_pgm_image(char *outfilename, unsigned char *image, int rows,
    int cols, char *comment, int maxval);

/* Adjust bright of imagein, retunrs image out*/
int bright(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int delta);

/* Function to binarize imagein, based on a given threshold, retunrs image out*/
int binarize(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int threshold);

/* Obtains the negative of imagein, returns imageout*/
int negative(unsigned char *imagein, int rows, int cols, unsigned char **imageout);

/* Equilize imagein histogram, returns imageout*/
int histogram_eq(unsigned char *imagein, int rows, int cols, unsigned char **imageout);

/* Applies an average filter of size*size dimensions to imagein, retunrs image out*/
int average(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int size);

/* Applies a median filter of size*size dimensions to imagein, retunrs image out*/
int median(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int size);

/* Applies a gaussian filter of size*size dimensions to imagein, retunrs image out*/
int gaussian(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int size, float stddev);

/* Applies an 90° isotropic filter to sharp edges of imagein, retunrs image out*/
int isotropic90(unsigned char *imagein, int rows, int cols, unsigned char **imageout);

/* Applies an 45° isotropic filter to sharp edges of imagein, retunrs image out*/
int isotropic45(unsigned char *imagein, int rows, int cols, unsigned char **imageout);

/* Function to calculate the absolute diference beetwen two imput images (must be same size)*/
int absdiff(unsigned char *imagein,int rows,int cols,unsigned char *imagein2, unsigned char **error);

/* Function to calculate mean saquare error between two images (must be same size)*/
double erms(unsigned char *imagein,int rows,int cols,unsigned char *imagein2);
 
/* End of File */
