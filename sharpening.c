/**********************************************************************
				
				 sharpening.c

	 This file contains functions/filters to sharp images 
	 * isotropic90 filter
	 * isotropic45 filter
	 
	 All functions takes an input image and return an output proccesed
	 image of same dimensions 

**********************************************************************/
/*Include Libraries*/
#include "Functions.h"

/* Applies an 90° isotropic filter to sharp edges of imagein, retunrs image out*/
int isotropic90(unsigned char *imagein, int rows, int cols, unsigned char **imageout){

	/*Kernel for isotropic filters*/
	int kernel[9]={0, -1, 0, -1, 5, -1, 0, -1, 0};
	int j, i, ik, jk, pixel, sum, in, jn;
		
	/* Allocate memory for output image. */
	if(((*imageout) = (unsigned char *) malloc((rows)*(cols))) == NULL){
	  	printf("Memory allocation failure.\n");
	  	return(0);
	}
	
    	/*Writes new pixel values to output image*/
    	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			/*Verify if current pixel has space to apply kernel, if not, copy same pixel value*/
			if(i<1 || i>=(rows-1) || j<1 || j>=(cols-1)){
 				(*imageout)[i*cols+j]=imagein[i*cols+j];
			} else {
				/*Apply kernel to current pixel*/
				sum=0;
				for (ik=0;ik<3;ik++){
					for (jk=0;jk<3;jk++){
						in=i+ik-1;
						jn=j+jk-1;
						pixel = imagein[in*cols+jn]*kernel[ik*3+jk];
						sum= pixel+sum;	
					}
				}			
				printf("\n");
				if(sum>PIXEL_MAX_VAL){sum=PIXEL_MAX_VAL;}
				else if (sum<PIXEL_MIN_VAL){sum=PIXEL_MIN_VAL;}
				(*imageout)[i*cols+j]=(unsigned char)(sum);
			}
		}
	}
	return(1);
}

/* Applies an 45° isotropic filter to sharp edges of imagein, retunrs image out*/
int isotropic45(unsigned char *imagein, int rows, int cols, unsigned char **imageout){

	/*Kernel for isotropic filters*/
	int kernel[9]={-1, -1, -1, -1, 9, -1, -1, -1, -1};

	int j, i, ik, jk, pixel, sum, in, jn;
		
	/* Allocate memory for output image. */
	if(((*imageout) = (unsigned char *) malloc((rows)*(cols))) == NULL){
	  	printf("Memory allocation failure.\n");
	  	return(0);
	}
	
    	/*Writes new pixel values to output image*/
    	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			/*Verify if current pixel has space to apply kernel, if not, copy same pixel value*/
			if(i<1 || i>=(rows-1) || j<1 || j>=(cols-1)){
 				(*imageout)[i*cols+j]=imagein[i*cols+j];
			} else {
				/*Apply kernel to current pixel*/
				sum=0;
				for (ik=0;ik<3;ik++){
					for (jk=0;jk<3;jk++){
						in=i+ik-1;
						jn=j+jk-1;
						pixel = imagein[in*cols+jn]*kernel[ik*3+jk];
						sum= pixel+sum;	
					}
				}			
				printf("\n");
				if(sum>PIXEL_MAX_VAL){sum=PIXEL_MAX_VAL;}
				else if (sum<PIXEL_MIN_VAL){sum=PIXEL_MIN_VAL;}
				(*imageout)[i*cols+j]=(unsigned char)(sum);
			}
		}
	}
	return(1);
}
/* End of File */
