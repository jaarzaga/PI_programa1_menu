/**********************************************************************
				
				 functions.c

	 This file contains basic functions to process images 
	 * bright - to change bright
	 * tresholding - to binarize the image
	 * negative - to get the negative of image
	 * histogram_eq - to equilize image histogram
	 
	 All functions takes an input image and return an output proccesed
	 image with same dimensions 

**********************************************************************/
/*Include Libraries*/
#include "Functions.h"

/* Adjust bright of imagein, retunrs image out*/
int bright(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int delta){
    int j, i, pixel;
    
	/* Allocate memory to store the image. */
	if(((*imageout) = (unsigned char *) malloc((rows)*(cols))) == NULL){
	  printf("Memory allocation failure.\n");
	  return(0);
	}
	
    /*Writes new pixel values to output image*/
    for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			pixel = imagein[i*cols+j]+delta;
			if(pixel>PIXEL_MAX_VAL){pixel=PIXEL_MAX_VAL;}
			else if (pixel<PIXEL_MIN_VAL){pixel=PIXEL_MIN_VAL;}
			(*imageout)[i*cols+j]=pixel;
		}
	}
   
    return(1);
}

/* Function to binarize imagein, based on a given threshold, retunrs image out*/
int binarize(unsigned char *imagein, int rows, int cols, unsigned char **imageout, int threshold){
    int j, i, pixel;
    
	/* Allocate memory to store the image. */
	if(((*imageout) = (unsigned char *) malloc((rows)*(cols))) == NULL){
	  printf("Memory allocation failure.\n");
	  return(0);
	}

	/*Review if threshold value is within the image pixel values*/
	if(threshold>PIXEL_MAX_VAL || threshold<PIXEL_MIN_VAL){
		printf("Invalid threshold value");
		return(0);
	}
	
    /*Writes new pixel values to output image*/
    for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			pixel = imagein[i*cols+j];
			if(pixel>=threshold){pixel=PIXEL_MAX_VAL;}
			else {pixel=PIXEL_MIN_VAL;}
			(*imageout)[i*cols+j]=pixel;
		}
	}
   
    return(1);
}

/* Obtains the negative of imagein, returns imageout*/
int negative(unsigned char *imagein, int rows, int cols, unsigned char **imageout){
    int j, i, pixel;
    
	/* Allocate memory to store the image. */
	if(((*imageout) = (unsigned char *) malloc((rows)*(cols))) == NULL){
	  printf("Memory allocation failure.\n");
	  return(0);
	}
	
    /*Writes negative value of each pixel values to output image*/
    for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			pixel = PIXEL_MAX_VAL - imagein[i*cols+j];
			(*imageout)[i*cols+j]=pixel;
		}
	}   
    return(1);
}

/* Equilize imagein histogram, returns imageout*/
int histogram_eq(unsigned char *imagein, int rows, int cols, unsigned char **imageout){
    int j, i, pixel_val,histin[PIXEL_MAX_VAL+1],histout[PIXEL_MAX_VAL+1];
    long int acum = 0;
    double hist=0, hist2, hist3;
    
	/* Allocate memory to store the image. */
	if(((*imageout) = (unsigned char *) malloc((rows)*(cols))) == NULL){
	  printf("Memory allocation failure.\n");
	  return(0);
	}
	
	/*Initilize histograms to 0*/
	for(i=PIXEL_MIN_VAL;i<=PIXEL_MAX_VAL;i++){histin[i]=0;}
	for(i=PIXEL_MIN_VAL;i<=PIXEL_MAX_VAL;i++){histout[i]=0;}
		
	/*Gets histogram of input image*/
	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			pixel_val = imagein[i*cols+j];
			histin[pixel_val]++;
		}
	}
	
	/**/
	/*Gets equilized histogram*/	
	for(i=PIXEL_MIN_VAL;i<=PIXEL_MAX_VAL;i++){
		acum = acum + histin[i];
		hist=((double)((PIXEL_MAX_VAL-PIXEL_MIN_VAL+1)/(double)(rows*cols)))*((double)acum+PIXEL_MIN_VAL-1);
		histout[i]=(int)hist;
	}
	
	/*Map the new equilized values on imageout from the original imagein*/

	for(i=0;i<rows;i++){
		for(j=0;j<cols;j++){
			pixel_val = imagein[i*cols+j];
			(*imageout)[i*cols+j]=histout[pixel_val];
		}
	}   
	
    return(1);
}
/* End of File */
