/**********************************************************************
				
				 FILE IMAGE_IO.C

	 This file contains functions to read information from tha input
       image file and to write output image files in PGM format.

**********************************************************************/
/*Include Libraries*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Functions.h"

/* Read the contents of the input image file in PGM format*/
int read_pgm_image(char *infilename, unsigned char **image, int *rows, int *cols)
{
   FILE *fp;
   char buf[71];
    
   /* Open the input image file for reading*/ 
   if((fp = fopen(infilename, "r")) == NULL)
   {
      fprintf(stderr, "Error reading the file %s.\n", infilename);
      return(0);
   }
   
   /* Verify that the image is in PGM format */
   fgets(buf, 70, fp);
   if(strncmp(buf,"P5",2) != 0)
   {
      fprintf(stderr, "The file %s is not in PGM format.\n", infilename);
      if(fp != stdin) 
         fclose(fp);
      return(0);
   }
   
   /* skip all comment lines */
   do{ fgets(buf, 70, fp); }while(buf[0] == '#');

   /* Read in the number of columns and rows in the image */
   sscanf(buf, "%d %d", cols, rows);
   
   /* skip all comment lines */
   do{ fgets(buf, 70, fp); }while(buf[0] == '#');

   /* Allocate memory to store the image then read the image from the file. */
   if(((*image) = (unsigned char *) malloc((*rows)*(*cols))) == NULL)
   {
      fprintf(stderr, "Memory allocation failure.\n");
      fclose(fp);
      return(0);
   }
   
   if((*rows) != fread((*image), (*cols), (*rows), fp))
   {
      fprintf(stderr, "Error reading the image data.\n");
      fclose(fp);
      free((*image));
      return(0);
   }

   fclose(fp);
   return(1);
}

/* Write the output image file in PGM format */
int write_pgm_image(char *outfilename, unsigned char *image, int rows,
    int cols, char *comment, int maxval)
{
   FILE *fp;

   /* Open the output image file for writing */
   if((fp = fopen(outfilename, "w")) == NULL)
   {
      fprintf(stderr, "Error writing the file %s.\n", outfilename);
      return(0);
   }

   /* Write the header information to the PGM file. */
   fprintf(fp, "P5\n%d %d\n", cols, rows);
   if(comment != NULL)
      if(strlen(comment) <= 70) 
         fprintf(fp, "# %s\n", comment);
   fprintf(fp, "%d\n", maxval);

   /* Write the image data to the file. */
   if(rows != fwrite(image, cols, rows, fp))
   {
      fprintf(stderr, "Error writing the result image data.\n");
      fclose(fp);
      return(0);
   }

   fclose(fp);
   return(1);
}

/* End of File */
