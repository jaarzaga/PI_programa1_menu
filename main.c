/**********************************************************************
				
				 Tarea1.c	
	 Uses functinos of image_io.c to read a PGM file and save its 
	 content in a diferent file .

**********************************************************************/
/*Include Libraries*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Functions.h"

int  main(int argc, char *argv[])
{ 
	int cols=0, rows=0, rows2=0, cols2=0, option, size,  result=0;
	unsigned char *imagein,*imagein2,*imageout;
	char filename[50], buf[71];
	float stddev;
	double error=0;
	FILE *fp;
	
	printf("Start of program\n");
	printf("Type (without .pgm) the name of the file to open (+ default lenna.pgm): ");
	scanf("%s", filename);

	/*Verify if defaul name was selected*/ 
	if(strncmp(filename,"+",1)==0){strcpy(filename, "lenna");}

	/*Read image file */
	strcat(filename,".pgm");
	if (read_pgm_image(filename, &imagein, &rows, &cols) == 0){
		return 0;
	}

	/*Print selection menu*/
	printf("***********************************************\n");
	printf("***********************************************\n");
	printf("** Basic functions                          ***\n");
	printf("**   1) Brightness adjustment               ***\n");
	printf("**   2) Trhesholding                        ***\n");
	printf("**   3) Negative                            ***\n");
	printf("**   4) Histogram equalization              ***\n");
	printf("** Softening                                ***\n");
	printf("**   5) Average                             ***\n");
	printf("**   6) Median                              ***\n");
	printf("**   7) Gaussian                             **\n");
	printf("** Sharpening                                **\n");
	printf("**   8) Isotropic 90                         **\n");
	printf("**   9) Isotropic 45                         **\n");
	printf("** Error calculation                         **\n");
	printf("**   10) Absoulte difference                 **\n");
	printf("**   11) Mean square error                   **\n");
	printf("***********************************************\n");
	printf("***********************************************\n\n");
    
	printf("Select an option from the menu: ");

	/*Call functions from menu, ask for aditional inputs if needed*/
	if(scanf("%d", &option)>0){
		switch(option){
			case 1:
				printf("Type a value from -255 to 255: ");
				if(scanf("%d", &size)>0){
					result = bright(imagein,rows,cols,&imageout,size);
				} else {
					printf("Invalid value");
				}
				break;
			case 2: 
				printf("Type a value from 0 to 255: ");
				if(scanf("%d", &size)>0){
					result = binarize(imagein,rows,cols,&imageout,size);
				} else {
					printf("Invalid value");
				}
				break;
			case 3:
				result = negative(imagein,rows,cols,&imageout);
				break;
			case 4:
				result = histogram_eq(imagein,rows,cols,&imageout);
				break;
			case 5:
				printf("Type the size N of kernel (NxN): ");
				if(scanf("%d", &size)>0){
					result = average(imagein,rows,cols,&imageout,size);
				} else {
					printf("Invalid value");
				}
				break;
			case 6:
				printf("Type the size N of kernel (NxN): ");
				if(scanf("%d", &size)>0){
					result = median(imagein,rows,cols,&imageout,size);
				} else {
					printf("Invalid value");
				}
				break;
			case 7:
				printf("Type the size N of kernel (NxN): ");
				if(scanf("%d", &size)>0){
					printf("Type the Standard deviation value: ");
					if(scanf("%f", &stddev)>0){
						result = gaussian(imagein,rows,cols,&imageout,size,stddev);
					} else {
						printf("Invalid value");
					}
				} else {
					printf("Invalid value");
				}
				break;
			case 8:
				result = isotropic90(imagein,rows,cols,&imageout);
				break;
			case 9:
				result = isotropic45(imagein,rows,cols,&imageout);
				break;
			case 10:
				printf("Type (without .pgm) the name of the second image: ");
				scanf("%s", filename);

				/*Read image file */
				strcat(filename,".pgm");
				if (read_pgm_image(filename, &imagein2, &rows2, &cols2) == 0){
					printf("Error opening the file");
				}
				if(rows!=rows2 || cols!=cols2){
					printf("Image are not the same size");
				}
				result = absdiff(imagein,rows,cols,imagein2,&imageout);
				break;
			case 11:
				printf("Type (without .pgm) the name of the second image: ");
				scanf("%s", filename);

				/*Read image file */
				strcat(filename,".pgm");
				if (read_pgm_image(filename, &imagein2, &rows2, &cols2) == 0){
					printf("Error opening the file");
				}
				if(rows!=rows2 || cols!=cols2){
					printf("Image are not the same size");
				}
				error = erms(imagein,rows,cols,imagein2);
				printf("The Mean Squre Error between images is %lf\n",error);
				result = -1;
				break;
			default:
				break;
		}
		
		/*Checks if there is any image to write out*/
		if(result>0){
			printf("Succesfully processed\n");
			/*Get filename and writes output image file*/
			printf("Type the name of the output file: ");
			scanf(" %s", filename);
			strcat(filename,".pgm");
			if(write_pgm_image(filename, imageout, rows, cols,NULL, PIXEL_MAX_VAL)==0){
				printf("Error writting the output file.\n\n");
			} else{
				printf("Succesfully writed\n\n");
			}
		}else if(result==0){printf("Failed during proccesing\n");}
	} else{
		printf("Invalid option.\n\n");
	}
	
	/*Release memory allocated for images*/
	free(imagein);
	free(imagein2);
	free(imageout);
	
    return 1;
}


